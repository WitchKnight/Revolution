{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_Revolution (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "C:\\Users\\Sweet Venom\\Documents\\Projects\\Sunwyrm\\Revolution\\.stack-work\\install\\fdf25839\\bin"
libdir     = "C:\\Users\\Sweet Venom\\Documents\\Projects\\Sunwyrm\\Revolution\\.stack-work\\install\\fdf25839\\lib\\x86_64-windows-ghc-8.0.2\\Revolution-0.1.0.0-JhrlfmC8elPHmDoEEGTelj"
dynlibdir  = "C:\\Users\\Sweet Venom\\Documents\\Projects\\Sunwyrm\\Revolution\\.stack-work\\install\\fdf25839\\lib\\x86_64-windows-ghc-8.0.2"
datadir    = "C:\\Users\\Sweet Venom\\Documents\\Projects\\Sunwyrm\\Revolution\\.stack-work\\install\\fdf25839\\share\\x86_64-windows-ghc-8.0.2\\Revolution-0.1.0.0"
libexecdir = "C:\\Users\\Sweet Venom\\Documents\\Projects\\Sunwyrm\\Revolution\\.stack-work\\install\\fdf25839\\libexec"
sysconfdir = "C:\\Users\\Sweet Venom\\Documents\\Projects\\Sunwyrm\\Revolution\\.stack-work\\install\\fdf25839\\etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Revolution_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Revolution_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "Revolution_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "Revolution_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Revolution_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Revolution_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "\\" ++ name)
