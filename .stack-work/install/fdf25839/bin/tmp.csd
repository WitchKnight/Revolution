<CsoundSynthesizer>

<CsOptions>

--output=dac --nodisplays

</CsOptions>

<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 1
0dbfs = 1.0
giPort init 1
opcode FreePort, i, 0
xout giPort
giPort = giPort + 1
endop




instr 21

endin

instr 20
 event_i "i", 19, 8.0, 1.0e-2
endin

instr 19
ir1 = 18
ir2 = 0.0
 turnoff2 ir1, ir2, ir2
 exitnow 
endin

instr 18
arl0 init 0.0
ir3 = 1.0
ar0 upsamp k(ir3)
ir4 = 0.125
ir5 = 0.0
ir6 = 392.0
ir7 = 313.6
ir8 = 250.88
ir9 = 200.704
ir10 = 240.8448
ir11 = 289.01376
ir12 = 346.816512
ir13 = 416.1798144
kr0 lpshold ir4, ir5, 0.0, ir6, ir3, ir7, ir3, ir8, ir3, ir9, ir3, ir10, ir3, ir11, ir3, ir12, ir3, ir13, ir3
ir15 = 1.0e-3
kr1 portk kr0, ir15
ar1 oscil3 ir3, kr1, 2
ar2 = (0.5 * ar1)
ir19 = 90.0
ir20 = 100.0
ar1 compress ar2, ar0, ir5, ir19, ir19, ir20, ir5, ir5, 0.0
ar0 = (ar1 * 0.8)
arl0 = ar0
ar0 = arl0
 out ar0
endin

</CsInstruments>

<CsScore>

f2 0 8192 10  1.0

f0 604800.0

i 21 0.0 -1.0 
i 20 0.0 -1.0 
i 18 0.0 -1.0 

</CsScore>



</CsoundSynthesizer>