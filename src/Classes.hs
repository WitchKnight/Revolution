 {-# LANGUAGE MultiParamTypeClasses,FlexibleInstances, InstanceSigs #-}
module Classes where

import Types
import Templates

import Debug.Trace
import Csound.Base
import Control.Lens
import qualified Data.Map.Lazy as DML


class SingleSignal a where
    gsg     :: Pitch -> a -> Sig

instance SingleSignal Note where
    gsg pitch (NT interv accent)    = mul vol $ pitch*interfrac
        where
            -- accfunc   = getFunc accent
            interfrac    =  getIntSig interv
            vol
                | accent == Silence = 0
                | otherwise = 1


class Playable a where
    plWith  :: (Sig -> Sig) -> Pitch -> a -> IO()
    pl      :: Pitch -> a -> IO()
    pl      = plWith osc
    dpl     :: a -> IO()
    dpl     = pl 440
    wtWithName  :: String -> (Sig -> Sig) -> Pitch -> a ->  IO()
    wtWith  :: (Sig -> Sig) -> Pitch -> a -> IO()
    wtWith  = wtWithName "Snd.wav"
    wt      :: Pitch -> a -> IO()
    wt      = wtWith osc
    
instance Playable Note where
    plWith instr pitch anote    = dac $ instr $ gsg pitch anote
    wtWithName name instr pitch anote = writeSnd name $ instr $ gsg pitch anote
instance Playable Node where
    plWith inst pitch nd        = dac $ mul 0.5 $ setDur adur $ inst $ getSeq pitch 1 nd
        where
            adur = int $  getDuration nd
    wtWithName name  inst pitch nd       = writeSnd name $ mul 0.5 $ setDur adur $ inst $ getSeq pitch 1 nd
        where
            adur = int $  getDuration nd

--- Deprecated, use only for debugging
class Playable a => PlayableTogether a where
    plWith' :: Instrument -> Pitch -> [a] -> IO()
    pl'     :: Pitch -> [a] -> IO()
    pl'     = plWith' baseOsc
    dpl'    :: [a] -> IO()
    dpl'    = pl' 440

instance PlayableTogether Node where
    plWith' instr pitch nds = dac $ mul 0.5 $ setDur adur $ sum [inst $ getSeq pitch 1 nd | nd <- nds]
        where
            inst = instr^.insfunc
            adur = int $ maximum[ getDuration nd |nd <- nds]

class SelfPlayable a where
    spl :: a -> IO()
    swt :: a -> IO()


instance SelfPlayable Score where
    spl sc = dac $ mul 0.5 $ setDur adur $ sum [let inst = i^.insfunc in inst $ getSeq pitch 1 nd | (_,(i,nd)) <- nds]
        where
            pitch = sig $ double $ sc^.center
            nds = DML.toList $ sc^.voices
            adur = int $ maximum [getDuration nd | (_,(_,nd)) <- nds]
    
    swt sc =  writeSnd name $ mul 0.5 $ setDur adur $ sum [let inst = i^.insfunc in inst $ getSeq pitch 1 nd | (_,(i,nd)) <- nds]
        where
            pitch = sig $ double $ sc^.center
            nds = DML.toList $ sc^.voices
            adur = int $ maximum [getDuration nd | (_,(_,nd)) <- nds]
            name = sc^.sconame

class (Transposable a) => Fractionable a where
    getFrac     :: a -> Double
    getIntSig   :: a -> Sig
    iflip       :: a -> a
    chUp        :: a -> a
    chDown      :: a -> a
    chDir       :: Direction -> a -> a
    simpl       :: a -> a
    isG         :: a -> Bool
    isL         :: a -> Bool
    isL a       = not $ isG a

instance Fractionable Interval where
    getFrac (PINT n m )= fromIntegral n / fromIntegral m
    getIntSig inter = sig $ double $ getFrac inter
    iflip (PINT n m) = PINT m n
    chUp (PINT n m) = PINT a b where (a,b) = (max m n , min m n)
    chDown i = iflip $ chUp i
    chDir dir = if dir==DUp then chUp else chDown
    simpl (PINT n m) =  PINT (n `quot` factor) (m `quot` factor)
        where factor = gcd n m
    isG (PINT a1 a2) = a1 >= a2

class Transposable a where
    transpose    :: Interval -> a -> a
    transpostack :: [Interval] -> a -> a
    transpostack xs a = foldr transpose a xs
    (.°)    ::  a -> Interval -> a
    (.°) a i = transpose (chUp i) a
    (°.)    ::  a -> Interval -> a
    (°.) a i = transpose (chDown i) a

instance Transposable Interval where
    transpose (PINT m n) (PINT m' n') = PINT (m*m') (n*n')

instance Transposable Note where
    transpose inter1 (NT inter2 acc) = NT (transpose inter1 inter2) acc

instance Transposable Pitch where
    transpose inter pitch = pitch*getIntSig inter

instance Transposable Node where
    transpose inter nd      = nd & interval %~ transpose inter

class Chainable a b where
    infixl 4 °+.
    (°+.)   :: a -> b -> [b]
    infixl 4 .+°
    (.+°)   :: a -> b -> [b]

instance Chainable Interval Interval where
    (°+.) int1 int2 = [int1, chDown int2]
    (.+°) int1 int2 = [int1, chUp int2]

instance Chainable [Interval] Interval where
    (°+.) ints int2 = ints ++ [chDown int2]
    (.+°) ints int2 = ints ++ [chUp int2]

class Revolution a where
    (./°°) :: a -> [(Interval,Int,Accent)] -> a
    (./°)  :: a -> (Interval,Int,Accent) -> a
    (./°) a ninf = (./°°) a [ninf]
    (./::) :: a -> [Interval] -> a
    (./::) a intervs = a ./°° [(interv, 1, Normal) | interv <- intervs ]
    (./:) :: a -> Interval -> a
    (./:) a interv = a ./::[interv]
    (./:?)  :: a -> Interval -> Int -> a
    (./:?) a interv dur = (./°°) a [(interv,dur, Normal)]
    (./::?) :: a -> [Interval] -> Int -> a
    (./::?) a intervs dur = a ./°° [(interv, dur, Normal) | interv <- intervs ]
    
instance Revolution Node where
    (./°°) a ninfs = addChildren [newNode x y z | (x, y, z) <- ninfs] a
        where inter = a^.interval
    
class Streamable a where --you treat the nodes as a stream of intervals
    -- join(add an independant node stream to the end of the node's stream) (ac) --
    (~+~)   :: a -> a -> a
    -- join multiple independant node streams
    (~+~~)  :: a -> [a] -> a
    njoin   :: [a] -> a
    
    -- chain a child node to the current node's stream 
    (~>~)   :: a -> a -> a
    (~>:)   :: a -> Interval -> a
    --chain multiple node streams
    (~>~~)  :: a -> [a] -> a 
    nchain  :: [a] -> a
    -- repeat the reseted stream n times 
    (~*~)   :: a -> Int -> a
    --chain the stream to itself n times 
    (~^~)   :: a -> Int -> a
    --reverse the stream WARNING ! REVERSES THE NODE STREAM NOT THE SOUND STREAM ! ALSO VERY COSTLY FOR LARGE STREAMS !
    streverse :: a -> a
    --add a reverse stream to the current stream
    (~<~)   :: a -> a -> a
    a ~<~ a' =  a ~>~ streverse a'
    (~<~~)  :: a -> [a] -> a
    a ~<~~ a's = a ~>~ foldr1 (~<~) a's
    -- removes the last element of the stream
    trim    :: a -> a
    -- removes n last elements of the stream
    trim'   :: Int -> a -> a
    trim' 1 a = trim a
    trim' n a = trim $ trim' (n-1) a
    --get the last node of a stream
    end     :: a -> a 

instance Streamable Node where
    (~+~) nd1 nd2   = addChildren [nd1,nd2] zeroNode
    (~+~~) nd ndls  = addChildren ndls nd
    njoin ndls = zeroNode ~+~~ ndls 
    (~>~) nd1 nd2
        | null nds  = addChild nd2 nd1
        | otherwise = nd1 & spawn %~ chainWithLast
        where
            nds = nd1^.spawn
            chainWithLast alist = let lastEl = last alist in reverse $  (lastEl ~>~ nd2)  : tail (reverse alist)
    (~>~~) n ndls = n ~>~ foldl1 (~>~) ndls
    (~>:) n inter = n ~>~ fromInterval inter
    nchain ndls = zeroNode ~>~~ ndls
    (~*~) nd n  = addChildren (replicate n nd) zeroNode
    (~^~) nd n
        | n < 2 = nd
        | otherwise = (nd ~>~ nd) ~^~ (n-1)
    trim nd = nd & spawn %~ clipLast
        where
            clipLast [] = []
            clipLast ndls
                | endLine lastEl = init ndls 
                | otherwise = reverse $  trim lastEl : tail (reverse ndls)
                where
                    lastEl = last ndls
    end nd
        | endLine nd    = nd
        | otherwise     = end $ last $ nd^.spawn
    streverse nd
        | endLine nd    = nd
        | otherwise     = end nd ~>~ streverse (trim nd)
  



class Fertile a where
    addChild :: a -> a -> a
    addChild a = addChildren [a]
    addChildren :: [a] -> a -> a
    
instance Fertile Node where
    addChildren ndls nd = nd & spawn %~ (++ ndls)

class Timed a where
    getDuration :: a -> Int

instance Timed Node where
    getDuration nd = sum (map getDuration $ nd^.spawn)+ nd^.size

class Sequenceable a where
    getSeq      :: Pitch -> Int -> a -> Sig
    getRevSeq   :: Pitch -> Int -> a -> Sig

instance Sequenceable Node where
    getSeq pitch n nd = constSeq (getSigSlices pitch nd) (sig $ int n)
    getRevSeq pitch n nd = constSeq (reverse $ getSigSlices pitch nd) (sig $ int n)

class Sliceable a where
    getSlices       ::  a -> [Note]
    getSigSlices    :: Pitch -> a -> [Sig]
    getSigSlices pitch a  = [gsg pitch x | x <- getSlices a]

instance Sliceable Node where
    getSlices :: Node -> [Note]
    getSlices nd = replicate times (NT sint acc) ++ concat [ getSlices $ transpose sint nd' | nd' <- nd^.spawn]
        where
            sint    = nd^.interval
            times   = nd^.size
            acc     = nd^.accent

toSig :: Double -> Sig
toSig a = sig $ double a

keepFirst :: (b->b) -> (a,b)-> (a,b)
keepFirst f (a,b)  = (a,f b)

keepSecond :: (b->b) -> (b,a) -> (b,a)
keepSecond f (b,a) = (f b,a)

changeSecond :: b -> (a,b) -> (a,b)
changeSecond b' (a,b) = (a,b')

changeFirst :: a -> (a,b) -> (a,b)
changeFirst a' (a,b) = (a',b)

applyBoth :: (a->a) -> (a,a) -> (a,a)
applyBoth f (a,b) = (f a, f b)
