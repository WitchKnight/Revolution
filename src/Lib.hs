module Lib
    ( 
    someFunc,
    zeroNode,
    node2,
    doubleIntervals
    ) where

import Types
import Classes
import Templates

import Debug.Trace
import Data.Map.Lazy as DML

someFunc :: IO ()
someFunc = putStrLn "someFunc"

node2 = node1 ~>~ fromInterval (chDown plainte) ~>~ fromInterval franche ~>~ fromInterval plainte
basicIntervals = [unisson,demipas,pas,grandpas,fauxpas,plainte,grave,luisante,franche,grande,éclat]
lowerIntervals = [chDown i | i <- basicIntervals]
doubleIntervals = basicIntervals ++ lowerIntervals