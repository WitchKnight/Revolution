{-# LANGUAGE TemplateHaskell #-}
module Types where

import Csound.Base
import Control.Lens
import Data.Map.Lazy 

type Pitch = Sig

data Interval = PINT Int Int deriving (Show,Read)

instance Eq Interval where
    (PINT n m) == (PINT n2 m2) = (fromIntegral n/fromIntegral m) == (fromIntegral n2/fromIntegral m2) 

data Direction = DDown | DUp  deriving (Show,Eq,Ord,Read)

data IntervalStack = IST [Interval] deriving (Show,Eq,Read)

data EType  =   Flat | Addit Int | Multipl Int deriving (Show,Eq,Read)

data Instrument = Instrument {
    _insname    :: String,
    _insID      :: Int,
    _insfunc    :: Sig -> Sig
}

baseOsc::Instrument
baseOsc = Instrument{
    _insname = "BaseOsc",
    _insID   = 0,
    _insfunc = osc
}

makeLenses ''Instrument

instance Show Instrument where
    show instr = show (_insname instr) ++ show (_insID instr)

instance Eq Instrument where
    instr1 == instr2 = (instr1^.insname == instr2^.insname && instr1^.insID == instr2^.insID)

data Accent = Normal | Staccato | Legato | Marcato | Silence deriving (Show,Eq,Read)

data Note = NT Interval Accent deriving (Show,Eq,Read) -- used in playing single nodes; used in node sequence

data Convenience = StopThere deriving (Show,Eq,Read)

data Special = Reverse deriving (Show,Eq,Read)

data NodeTag = ETag EType | CTag Convenience | STag Special deriving (Show,Eq,Read)

data Node = Node {
    _size       :: Int,
    _volume     :: Double,
    _accent     :: Accent,
    _interval   :: Interval,
    _spawn      :: [Node],
    _tags       :: [NodeTag]
} deriving(Eq,Show)
makeLenses ''Node

newNode :: Interval -> Int -> Accent -> Node
newNode interv dur acc = Node {
    _size       = dur,
    _volume     = 1,
    _accent     = acc,
    _interval   = interv,
    _spawn      = [],
    _tags       = []
}

fromNote :: Int -> Note -> Node
fromNote n (NT inter acc) = newNode inter n acc

fromNote' :: Note -> Node
fromNote' (NT inter acc) = newNode inter 1 acc

fromInterval :: Interval -> Node
fromInterval inter = fromNote' (NT inter Normal) 

zeroNode :: Node
zeroNode = newNode (PINT 1 1) 0 Silence

endLine :: Node -> Bool
endLine nd = nd^.spawn == []

type Voice = (Instrument,Node)

data Score = Score {
    _center     :: Double,
    _sconame    :: String,
    _voices     :: Map Int Voice
} deriving (Show,Eq)
makeLenses ''Score




