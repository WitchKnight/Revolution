module Templates where

import Types

import Csound.Base
import qualified Data.Map.Lazy as DML

unisson     = PINT 1 1
demipas     = PINT 16 15
pas         = PINT 10 9
grandpas    = PINT 9 8
fauxpas     = PINT 8 7
plainte     = PINT 7 6
grave       = PINT 6 5
luisante    = PINT 5 4
franche     = PINT 4 3
grande      = PINT 3 2
éclat       = PINT 2 1




note1 = NT unisson Normal

node1 = fromNote' note1

baseInstrument = Instrument {
    _insname = "wave oscillator",
    _insID = 1,
    _insfunc = osc
}

baseScore = Score {
    _center     = 440,
    _sconame    = "Revolution test",
    _voices     = DML.fromList [(0, (baseInstrument,zeroNode))]
}