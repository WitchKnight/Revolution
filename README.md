# Revolution

Revolution is a music library allowing to compose music without relying on a premade temperament; by either making your own (or loading common ones) or just composing with intervals on the fly. 
There are no fixed notes in Revolution, unless you want to.

-- usage instructions --

Revolution is intended to be used using stack repl (ghci) to compose on the fly.
you can however hardcode a function in the Main module if you so choose.

NixOs: install stack.
execute 'stack repl --nix' in the terminal

other Linux/ Windows 

1.install stack. I recommend installing the haskell platform (https://www.haskell.org/platform/)

2.install csound (using your package manager of choice or https://sourceforge.net/projects/csound/ )

edit the stack.yaml file in the project so that this bit is commented or deleted:

nix:
    enable:  true
    shell-file: shell.nix 

execute 'stack repl' in the terminal
