module UserInterface where

import Lib
import Types
import Classes
import Visual

import qualified Data.Map.Lazy as DML
import Control.Monad
import Control.Lens((^.))
import System.IO.Unsafe
import Data.IORef
import qualified Graphics.UI.Threepenny       as UI
import           Graphics.UI.Threepenny.Core



upi = unsafePerformIO

chainInterB :: IORef ScoreData ->  Element -> Element -> UI Element
chainInterB scd  num denom = do
    button <- UI.button # set UI.text ("chain interval !")
    on UI.click button $ const $ do
        n <- get value num
        d <- get value denom
        let inter = PINT (read n) (read d)
        liftIO $ modifyIORef scd $ mSN (~>: inter)
    return button

joinInterB :: IORef ScoreData ->  Element -> Element -> UI Element
joinInterB scd  num denom = do
    button <- UI.button # set UI.text ("join interval !")
    on UI.click button $ const $ do
        n <- get value num
        d <- get value denom
        let inter = PINT (read n) (read d)
        liftIO $ modifyIORef scd $ mSN (./: inter)
    return button

resetB :: IORef ScoreData ->  UI Element
resetB scd  = do
    button <- UI.button # set UI.text ("reset whole node ")
    on UI.click button $ const $ do
        liftIO $ modifyIORef scd $ cSN zeroNode
    return button
    
repeatB :: IORef ScoreData ->  UI Element
repeatB scd  = do
    button <- UI.button # set UI.text ("join to itself")
    on UI.click button $ const $ do
        liftIO $ modifyIORef scd $ mSN (~*~ 2)
    return button

chainSelfB :: IORef ScoreData ->  UI Element
chainSelfB scd  = do
    button <- UI.button # set UI.text ("chain to itself")
    on UI.click button $ const $ do
        liftIO $ modifyIORef scd $ mSN (~^~ 2)
    return button

playSB :: IORef ScoreData ->  UI Element
playSB scd = do
    (rsc,_) <- liftIO $ readIORef scd
    button <- UI.button # set UI.text "play the score."
    on UI.click button $ const $ do
        liftIO $ spl rsc 
    return (button)

drawScore ::IORef ScoreData -> Element -> UI Element
drawScore scd cv = do 
    (rsc,ed) <- liftIO $ readIORef scd
    forM_ (DML.toList $ rsc^.voices) $ \(_,(i,nd)) -> do
        let ipos = fromIntegral $ i^.insPrio * 50
        let nodepos = (0,ipos)
        let rects = makeRects' ipos $  nd
        let (w,h) = (12,12)
        cv # UI.fillText (i^.insname++" : ") nodepos 
        forM_  rects $  \(x,y,color) -> do 
            cv # set' UI.fillStyle (UI.htmlColor color)
            cv # UI.fillRect (x, y) w h 
    return cv
