module Main where

import Lib
import Types
import Classes
import Templates
import UserInterface

import Debug.Trace
import Safe(readMay)
import Control.Concurrent (forkIO)
import Data.IORef

import qualified Graphics.UI.Threepenny         as UI
import           Graphics.UI.Threepenny.Core
import Control.Lens((^.),(&),(.~),(%~))

cX = 800
cY = 300



main :: IO ()
main = do 
    sced <- newIORef (baseScore, ED 0)
    startGUI defaultConfig {
        jsPort      = Just 8023,
        jsStatic    = Just "..wwwroot"
    } (setup sced)

setup :: IORef ScoreData -> Window -> UI ()
setup score w = do
    return w #  set UI.title "Revolution"
    num     <-  UI.input # set value "1"
    denom   <-  UI.input # set value "1"
    (sc,ed) <- liftIO $ readIORef score
    freq    <-  UI.input # set value (show $ sc^.center)
    bIntC   <-  chainInterB score  num denom 
    bIntJ   <-  joinInterB score  num denom
    bSelfC  <-  chainSelfB score
    bSelfJ  <-  repeatB score 
    bSelfR  <-  resetB score 
    bPlay   <-  playSB score
    canvas  <-  UI.canvas
            # set UI.height cY
            # set UI.width  cX
            # set style [("border", "solid black 1px"), ("background", "#eee")]
    dCanvas <- drawScore score canvas
    changeFreq <- UI.button # set UI.text "change the base frequency"
    
    getBody w #+    [   row     [element freq, element changeFreq],
                        row     [element num,element denom],
                        row     [element bIntC ,element bIntJ],
                        row     [element bSelfC, element bSelfJ, element bSelfR],
                        column  [element dCanvas, element bPlay]
                    ]
        
    on UI.click changeFreq $ const $ do
        
        liftIO $ print $ sc^.center
        frq <- get value freq
        trace frq $ liftIO $ modifyIORef score $ setCenter frq
        

    -- drawNode canvas bnd
    return()