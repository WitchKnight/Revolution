{nixpkgs ? import <nixpkgs> { }, ghc ? nixpkgs.ghc}:

with nixpkgs;

haskell.lib.buildStackProject {
  name = "Revolution";
  buildInputs = [ 
   csound
 ];
  inherit ghc;
}
