<CsoundSynthesizer>

<CsOptions>

--nodisplays

</CsOptions>

<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 1
0dbfs = 1.0
giPort init 1
opcode FreePort, i, 0
xout giPort
giPort = giPort + 1
endop




instr 21

endin

instr 20
 event_i "i", 19, 12.0, 1.0e-2
endin

instr 19
ir1 = 18
ir2 = 0.0
 turnoff2 ir1, ir2, ir2
 exitnow 
endin

instr 18
arl0 init 0.0
ir3 = 1.0
ar0 upsamp k(ir3)
ir4 = 8.333333333333333e-2
ir5 = 0.0
ir6 = 440.0
ir7 = 488.8888888888889
ir8 = 543.2098765432098
ir9 = 579.4238683127572
ir10 = 643.8042981252858
ir11 = 321.9021490626429
ir12 = 357.6690545140477
ir13 = 397.4100605711641
ir14 = 423.9040646092417
ir15 = 471.0045162324908
ir16 = 235.5022581162454
kr0 lpshold ir4, ir5, 0.0, ir6, ir3, ir7, ir3, ir8, ir3, ir9, ir3, ir10, ir3, ir11, ir3, ir11, ir3, ir12, ir3, ir13, ir3, ir14, ir3, ir15, ir3, ir16, ir3
ir18 = 1.0e-3
kr1 portk kr0, ir18
ar1 oscil3 ir3, kr1, 2
ar2 = (0.5 * ar1)
ir22 = 90.0
ir23 = 100.0
ar1 compress ar2, ar0, ir5, ir22, ir22, ir23, ir5, ir5, 0.0
ar0 = (ar1 * 0.8)
arl0 = ar0
ar0 = arl0
 out ar0
endin

</CsInstruments>

<CsScore>

f2 0 8192 10  1.0

f0 604800.0

i 21 0.0 -1.0 
i 20 0.0 -1.0 
i 18 0.0 -1.0 

</CsScore>



</CsoundSynthesizer>